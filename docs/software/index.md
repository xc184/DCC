# About
The DCC support most commonly used open-source scientific software and tools for cluster and high performance computing.

Widely used software are installed as [modules](../software/modules.md) DCC adminstrators. Applications can be installed on request, email [rescomputing@duke.edu](mailto:rescomputing@duke.edu). Generally, applications should be open source and recommended for use on a computing cluster.

Users may also install software in their group directories. 
