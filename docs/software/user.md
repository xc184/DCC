# User installed software
Lab groups are welcome to install software in their `hpc/group/<groupname>` space if `sudo` access is not required.  This can be helpful for lab specific software or for when a group wants to strongly control software versions and packages.

## Miniconda installation sample
    mkdir -p /hpc/group/<groupname>/<netid>
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    sh Miniconda3-latest-Linux-x86_64.sh

and then follow the instructions. The place to give as Miniconda install location should be `/hpc/group/<groupname>/<netid>/miniconda3`. It will offer to update your ~/.bashrc, (init) which is what you want.   Log out log back in and then you can run conda install, pip install, create environments, etc.
