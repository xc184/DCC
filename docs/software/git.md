
New to version control and Git? Visit: [info on Duke's GitLab](https://oit-rc.pages.oit.duke.edu/docs/). Git is fully supported on the DCC and highly recommended for version control of your bash scripts, code, and to ease automation and deployment of your software environments using [containers](../software/containers.md).

DCC Lab groups can easily create a GitLab group through Research Toolkits to easily share repos within your group. See [instructions](https://oit-rc.pages.oit.duke.edu/docs/RT/).


HTTPS and SSH are both supported and can be used to connect to Git from the DCC. HTTPS is faster and easier to setup for quickly connecting and copying a repo to the DCC. When working with repos where you regularly push and pull updates, SSH will be easier to use as SSH keys will vastly simplify your login experience.

## Using HTTPS
1. Open a web browser and navigate to the Git repository you want to connect to.
2. Click on the "Clone" button in the top right corner of the repository page.
3. In the "Clone with HTTPS" section, copy the URL by clicking the "Copy" button.
4. Open a terminal or command prompt and navigate to the directory where you want to clone the repository.
5. Run `git clone <url>` to clone the repository. If prompted, enter your GitLab username and password to authenticate the clone.

Once the repository is cloned, you can make changes, commit, and push as normal.

## Using SSH

### Generating SSH keys
1. Open the terminal
2. Type `ssh-keygen` and press enter
3. Press enter to use the default file location
4. Enter a passphrase and press enter

You should now have a private key (id_rsa) and a public key (id_rsa.pub) in the default ~/.ssh directory

### Adding an SSH key to Git
1. Go to your GitHub/GitLab profile settings
2. Select "SSH Keys" from the left sidebar
3. Click the "Add SSH Key" button
4. Give your key a title (e.g. "DCC")
5. Open the id_rsa.pub file in a text editor and copy the contents
6. Paste the contents into the "Key" field in GitLab
7. Click the "Add key" button

You should now be able to use SSH to access GitLab
Note: if you encounter any problem with ssh-keygen, you may want to run ssh-keygen -t rsa -b 4096

### Connecting to GitLab/GitHub Using SSH

1. Open a web browser and navigate to the Git repository you want to connect to.
2. Click on the "Clone" button in the top right corner of the repository page.
3. In the "Clone with SSH" section, copy the URL by clicking the "Copy" button.
4. Open a terminal or command prompt and navigate to the directory where you want to clone the repository.
5. Run `git clone <url>` to clone the repository. If prompted, enter your ssh key passphrase to authenticate the clone.

Once the repository is cloned, you can make changes, commit, and push as normal.