# Modules
Software is provided on the DCC under the form of loadable environment modules. The use of a module system means that most software is not accessible by default and has to be loaded using the module command. This mechanism allows us to provide multiple versions of the same software concurrently, and gives users the possibility to easily switch between software versions. The modules system helps setting up the user's shell environment to give access to applications, and make running and compiling software easier.

When you first login, you'll be presented with a default, bare bones environment with minimal software available. The module system is used to manage the user environment and to activate software packages on demand. In order to use software installed, you must first load the corresponding software module.

When you load a module, the system will set or modify your user environment variables to enable access to the software package provided by that module. For instance, the `$PATH` environment variable might be updated so that appropriate executables for that package can be used.

Run `module avail` to see the list of installed applications

Run `module load (module name)` to use the application

You can add the `module load` command to your job scripts, or to the end of the .bash_profile file in your home directory.
