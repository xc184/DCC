# DCC Community Software

Do you have or use software on the DCC that would benefit the larger DCC community?  We are happy to host you!  We will provide the hosting location and storage, give you access to manage the install/files, and post your info to this site.  [Contact us](../index.md) for information.

## OnDemand
### RStudio Interactive Servers
Additional RStudio Interactive Servers can be created by forking the [base OnDemand RStudio project](https://gitlab.oit.duke.edu/OIT-DCC/bc_stats_rstudio). 

#### Microbiome 
Created by Josh Granek at the [Duke Center for Human Systems Immunology](https://sites.duke.edu/chsi/) to assist with bioinformatics support for the microbiome community at Duke. This app will launch an RStudio server from a Microbiome focused Singularity image on one or more nodes.

[OnDemand GitLab Project](https://gitlab.oit.duke.edu/chsi-informatics/bc_microbiome_rstudio)

1. [RStudio Microbiome Singularity Recipe](https://gitlab.oit.duke.edu/dmc/microbiome-bioinformatics-image/-/tree/main)


## DCC

### Alphafold2
Thanks to the [Duke Human Vaccine Institute](https://dhvi.duke.edu), Alphafold 2.2.0 from [Deep Mind](https://deepmind.com) is installed inside of a singularity container on the DCC.

Sample job submission scripts for Alphafold Monomer and Alphafold Multimer predictions are provided at:

    /opt/apps/community/alphafold2/alphafoldv2.2/scripts

To use Alphafold, first copy the sample submission script to your group directory, then using the comments, edit the submission script.  

To connect with the Duke Alphafold2 Community [enroll](https://lists.duke.edu/sympa/info/dccusers_alphafold2) in the community listserve.
