## DCC Workshops
We recommend that all DCC users sign up for and attend a introductory session to the DCC.  The session is held on the first Tuesday of every month, register at [colab.duke.edu/roots/](https://pathways.duke.edu/modulepage/144) for an upcoming session.  [Current pdf version of the slides.](https://duke.box.com/s/zosu3w34iap7o4yuvlq44zep8ic27byq)
## Other Live RC Workshops
Research Computing offers in person and virtual technical training workshops through the Innovation Co-Lab /roots program. Topics vary each semester and range from introductory skills to advanced topics for senior researchers. View the full list of topics and register for a research computing workshop at: [colab.duke.edu/roots](https://pathways.duke.edu/trackpage/31)

Workshops include: Linux & the BASH shell, Python, R, Singularity

Additionally, Duke users may register for XSEDE Webinars at: [portal.xsede.org/course-calendar/](https://portal.xsede.org/course-calendar/)

## Recommended Online Training for Newer Users
The Duke co-lab /roots program offers:

* [Linux & the BASH Shell Self-Paced Course](https://pathways.duke.edu/modulepage/142) 
* [Git Self-Paced Course](https://pathways.duke.edu/modulepage/135) 

Software carpentry has a number of online lessons for basic lab skills for research computing at [software-carpentry.org](https://software-carpentry.org/):

* [The UNIX shell](https://swcarpentry.github.io/shell-novice/)
* [Version Control with GIT](https://swcarpentry.github.io/git-novice/)
* [Programming with Python](https://swcarpentry.github.io/python-novice-inflammation/)
* [Plotting and Programming in Python](http://swcarpentry.github.io/python-novice-gapminder/)
* [Programming with R](http://swcarpentry.github.io/r-novice-inflammation/)
* [R for Reproducible Scientific Analysis](https://swcarpentry.github.io/r-novice-gapminder/)
* [Intro to HPC](https://carpentries-incubator.github.io/hpc-intro/) - this course is under-development, but is an excellent overview of cluster computing for novices.






