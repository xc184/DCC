# About
In addition to these resources, we offer monthly onboarding sessions for new users, as well as training on more technical topics throughout the semester.  See our training.

For questions, sending an email to [rescomputing@duke.edu](mailto:rescomputing@duke.edu) is always a good first step.  Please do your best to include your question along with any actual commands and output you are seeing. 

Use office hours to get more help on using the Duke Compute Cluster efficiently or working through tougher issues.  We love to meet you and hear what you are working on.  You can book office hours directly with us via Zoom through this [online calendar.](https://outlook.office365.com/owa/calendar/ResearchComputing@ProdDuke.onmicrosoft.com/bookings/)

We look forward to meeting you. Happy computing!


This site is produced and managed by the [Research Computing Interns](interns.md).