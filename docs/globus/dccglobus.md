# Transferring a file from your local computer to the DCC

## Introduction

With the help of Globus, there is a very easy process to transfer files from one of your local computers to the DCC. This page serves as a guide on how to do so.

## Preparing your local computer

1. Download and install Globus Connect Personal on your personal computer using the following links: [macOS](https://downloads.globus.org/globus-connect-personal/v3/mac/stable/globusconnectpersonal-latest.dmg), [Windows](https://downloads.globus.org/globus-connect-personal/v3/windows/stable/globusconnectpersonal-latest.exe), or [Linux](https://downloads.globus.org/globus-connect-personal/v3/linux/stable/globusconnectpersonal-latest.tgz)
2. Open Globus Connect Personal and click "Log In"

    ![!](../assets/dccglobus/login_app.png)

3. Using the browser window that opens, log into Globus using your Duke credentials

    ![!](../assets/dccglobus/login_website.png)

4. Allow Globus Connect Personal Setup all permissions requested

    ![!](../assets/dccglobus/permissions_requested.png)

5. Return to the Globus Connect Personal application to complete setup
6. Use your Duke credentials for "Owner Identity" and create a name like "Personal Computer" for "Collection Name", then click "Save"

    ![!](../assets/dccglobus/collection_details.png)

7. Click "Exit Setup" to complete the setup process

    ![!](../assets/dccglobus/exit_setup.png)

## Transferring files

1. Visit and log into [https://app.globus.org/file-manager](https://app.globus.org/file-manager) to begin transferring files
2. Next to "Collection", click "Search"

    ![!](../assets/dccglobus/first_search.png)

3. Navigate to the "Your Collections" tab and click "Personal Computer" or whatever name you selected in step 6 above

    ![!](../assets/dccglobus/personal_computer_selection.png)

4. Using the file navigation menu, select the files you wish to transfer, then click "Transfer or Sync to..."

    ![!](../assets/dccglobus/file_selection.png)

5. Click "Search" in the top right box

    ![!](../assets/dccglobus/second_search.png)

6. Search for "Duke Compute Cluster (DCC) Data Transfer Node" and select it

    ![!](../assets/dccglobus/transfer_node_search.png)

7. Navigate to your desired transfer destination directory on the DCC (right pane) and then click "Start" on the left pane

    ![!](../assets/dccglobus/start_transfer.png)

8. Click the "refresh list" icon on the right pane to confirm your files have transferred; you should also receive an email confirmation

    ![!](../assets/dccglobus/final_results.png)

9. Your files are now on the DCC and ready for use!

## Additional remarks

The "Duke Compute Cluster (DCC) Data Transfer Node" supports data transfers to and from the DCC.  The name is a misnomer as **any** OIT provided storage can be mounted to this node for use with Globus. 

By current standard practice all shares that are on the DCC (home, group, work, and data commons) are also mounted to the Globus Duke Compute Cluster (DCC) data transfer node using the same group permissions.

If you need a share not on the DCC mounted or additional rights to share data on globus, contact us.
