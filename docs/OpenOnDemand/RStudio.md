# Using the RStudio server
1. Click on Interactive Apps in the top navigation menu
2. Click on RStudio

## Launching an RStudio server
1. Select your partition from the dropdown 
2. Select the name of your DCC group.  If you are a member of multiple groups, make sure the group you select has access to the partition you select.
3. Input the number of hours you would like the server to remain active (please try to remain small, as it will continue running even if you are not using it)
4. Input the desired amount of nodes, memory, CPUs, and GPUs (try to start small with only a few gigabytes of memory and cores).  The higher the requirements you select, the longer your wait times.
5. Optionally, enter a Datacommons mount path and/or any additional Slurm parameters
6. Use the "Select Path" button to select the base of installed R packages
7. Press the blue "Launch" button on the bottom of the page

## Connecting to RStudio 
1. After pressing the blue "launch" button, your job will be queued to start an RStudio server. You should see this automatically
2. Wait a few seconds to a few minutes for the RStudio server to finish launching. The status will automatically change from "Starting" to "Running" when the server is ready
3. Press the blue "Connect to RStudio Server" button when the server is running to access your RStudio server

## Using RStudio Server
1. In the top left of the interface, click on "File" > "New File" > "R Script" to create a new R Script
2. To save this file, use Ctrl+S (Command+S on macOS) and choose a file path
3. Alternatively, upload your existing .r files using the "Upload" button toward the top of the pane in the bottom left corner
4. When you are ready, you can run your R Script by pressing the "Run" button toward the top right of the top left pane

## Packages available in RStudio
Several pre-built options are available through RStudio server to support a variety of package installations.  Once you select your option, you may also install additional packages.

Image Name | R Version | About | Build Repo
-----|------|-------------|--------
`jags.sif`| 4.1.1 | [JAGS](https://sourceforge.net/projects/mcmc-jags/) is Just Another Gibbs Sampler. It is a program for the statistical analysis of Bayesian hierarchical models by Markov Chain Monte Carlo. | [JAGS Singularity Recipe](https://gitlab.oit.duke.edu/OIT-DCC/jags)
`rstudio-rstan.sif`| 4.1.1 | [RStan](https://mc-stan.org/users/interfaces/rstan) and supporting/common packages | [RSTAN Singularity Recipe](https://gitlab.oit.duke.edu/OIT-DCC/rstudio-rstan)
`ondemand_bioconductor.sif`| Bioconductor is version 3.14; R version 4.1.0 | This image is based on the [Microsoft Docker Bioconductor image](https://hub.docker.com/_/microsoft-bioconductor-bioconductor-docker), [more about Bioconductor](https://www.bioconductor.org) | [Bioconductor Singularity Recipe](https://gitlab.oit.duke.edu/OIT-DCC/bioconductor)


### Installing your own packages
Coming Soon