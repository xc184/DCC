# Using the Jupyter Lab server

![!](../assets/jupyter/initial_navigation.png)

1. Click on Interactive Apps in the top navigation menu
2. Click on Jupyter Lab

## Launching a Jupyter Lab server

![!](../assets/jupyter/launching_a_server.png)

1. For lab account, input the name of your DCC group (list of all groups can be found [here](https://dcc-ondemand-01.oit.duke.edu/pun/sys/dashboard/files/fs//hpc/group))
2. Under partition, type in "common", or if your lab has dedicated resources, add your own partition. You may also use common-gpu or scavenger-gpu if you need GPU resources. (remember, if a GPU is not available you may not get your interactive session in a timely fashion)
3. Input the number of hours you would like the server to remain active (please try to remain small, as it will continue running even if you are not using it)
4. Input the desired amount of nodes, memory, and CPUs (try to start small with only a few gigabytes of memory and cores)
5. Enter any additional Slurm parameters (this is optional). If you would like to request a GPU, make sure the partition you have selected has GPU resources, and add ` --gres=gpu:1` under “additional slurm parameters”
6. Press the blue "Launch" button on the bottom of the page

## Connecting to Jupyter

![!](../assets/jupyter/connecting_to_jupyter.png)

1. After pressing the blue "launch" button, your job will be queued to start a Jupyter Lab server. You should see this automatically
2. Wait a few seconds to a few minutes for the Jupyter Lab server to finish launching. The status will automatically change from "Starting" to "Running" when the server is ready
3. Press the blue "Connect to Jupyter" button when the server is running to access your Jupyter Lab server

## Using Jupyter Lab

![!](../assets/jupyter/using_jupyter.png)

1. Click on Python3 under "Notebook" to create a new .ipynb notebook
2. Alternatively, upload your existing .ipynb files using the pane on the left-hand side
3. You can drag-and-drop or press the upward facing arrow to upload files. 
Note: the Jupyter session defaults to file browsing in your home directory.  To browse to your group directory, first (in a terminal window) create a symbolic link in your home directory.
`ln -s /hpc/group/<groupname> <groupname>`
4. When you are ready, you can run your Jupyter Notebook by pressing the run button at the top of the .ipynb file window

![!](../assets/jupyter/running_a_notebook.png)
