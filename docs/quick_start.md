# Quick  Start

##  Getting a DCC Account
To get access to the Duke Compute Cluster (DCC), Duke researchers can contact their group or lab's point of contact, who manage DCC membership through a [self-service portal](https://rtoolkits.web.duke.edu). Faculty researchers can set up a new group by contacting the DCC team, and any Duke netid user can be added to the DCC.[contact us](https://dcc.duke.edu/help/)
##  Logging into the DCC
- Logging use Web portal - [OnDemand](https://dcc.duke.edu/dcc/login/)
- Logging use Terminal - [SSH Connection](https://dcc.duke.edu/dcc/login/)

##  Schedule a Job
 You can start your jobs in one of two ways.
- Run Jobs with Slurm
- Run Jobs with interactive Apps on OneDemand
## Set up environment 
- Set up environment for Python
- Set up environment for R 
## Transfer your files
- Transfer your file interactively using OnDemand
- Transfer your file from Gitlab/Github
- Command-Line Transfer Tools
- Big data 
## Q&A
Mostly asked questions and solutions can be found [here]().
## Get help
- If you have additional questions or comments, we offer a variety of ways to help [here](https://dcc.duke.edu/help/).
