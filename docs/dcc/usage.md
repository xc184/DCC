## Cluster Usage Reporting

View real-time job accounting on the cluster with [sacct](https://slurm.schedmd.com/sacct.html) or job reporting for the last 30 days with [sreport](https://slurm.schedmd.com/sreport.html).

Historical job accounting is available to DCC users through Tableau.

All job data is reported on the start day of the job (important for long running jobs).

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type='text/javascript' src='https://tableau.oit.duke.edu/javascripts/api/viz_v1.js'></script>
<div id="vizDiv" class='tableauPlaceholder'>
   <object id="viz" class='tableauViz' width='100%' height='1050' style='display:none;'>
      <param name='host_url' value='https%3A%2F%2Ftableau.oit.duke.edu%2F' />
      <param name='embed_code_version' value='3' />
      <param name='site_root' value='&#47;t&#47;prod' />
      <param name='name' value='RCDCCSLURMReports&#47;ResourcesUsedbyGroup' />
      <param name='tabs' value='yes' />
      <param name='toolbar' value='yes' />
      <param name='showAppBanner' value='false' />
   </object>
</div>
<script>
    let width = document.getElementsByClassName("md-main")[0].offsetWidth;
    console.log(document.getElementsByClassName("md-sidebar__scrollwrap")[0].offsetWidth)
    width -= document.getElementsByClassName("md-sidebar__scrollwrap")[0].offsetWidth;
    width -= document.getElementsByClassName("md-content")[0].offsetWidth;
    width /= 3;
    width += document.getElementsByClassName("md-content")[0].offsetWidth;
    console.log(width.toString())
    document.getElementById("vizDiv").setAttribute("width", width.toString())
    document.getElementById("viz").setAttribute("width", width.toString())
    console.log(width)
</script>

If you are member of multiple lab groups and need to update your default SLURM account for reporting information, please use:

`sacctmgr modify user u=(NetID) set DefaultAccount=(account name)`