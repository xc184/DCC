# Logging into the DCC

As a shared computing resource, all work done on the DCC must be done by submitting jobs to the [scheduler](slurm.md). Login nodes must not be used to execute computing tasks. 

Acceptable use of login nodes include:

* lightweight file transfers,
* script and configuration file editing,
* job submission and monitoring,
* [BaseSpace BaseMount](https://basemount.basespace.illumina.com) - users can use basemount to mount BaseSpace into their home directories.

To minimize disruption and ensure a comfortable working environment for users, resource limits are enforced on login nodes, and processes started there will automatically be terminated if their resource usage (including CPU time, memory and run time) exceed those limits.
## Web Portal - Open OnDemand
For  new  users we recommend using the web portal, you can access the clusters by  open a GUI interface on the OnDemond (such as RStudio or Jupyter), use the [Open OnDemand service.](https://dcc-ondemand-01.oit.duke.edu/pun/sys/dashboard)
For more information about [OnDemand](../OpenOnDemand/index.md)

## SSH Connection
For more advanced use cases that not well supported by OnDemand you can connect using SSH.

Login to the DCC using `ssh netid@dcc-login.oit.duke.edu`, (note: VPN is not required, but MFA **is required**). 

    netid@node ~ % ssh netid@dcc-login.oit.duke.edu
    
    Password: 
    Duo two-factor login for netid

    Enter a passcode or select one of the following options:

    1. Duo Push to XXX-XXX-4007
    2. Phone call to XXX-XXX-4007
    3. Phone call to XXX-XXX-9784
    4. SMS passcodes to XXX-XXX-4007 (next code starts with: 2)

    Passcode or option (1-4): 1
    Success. Logging you in...
    Last login: Tue Dec 21 11:07:41 2021 from xxx
    ################################################################################
    # MOTD                                                                         # 
    # My patch window is wednesday 03:00                                           # ################################################################################
    netid8@dcc-login-03  ~ $

### A word on ssh clients
Linux and MacOS systems generally come with a SSH client already installed and you can `ssh` directly from a terminal window. For Windows users you will need an ssh client such as [PuTTY](https://putty.org). Advanced users may consider installing WSL (Windows Subsystem for Linux), see [installation instructions here](https://docs.microsoft.com/en-us/windows/wsl/install), but beware DCC support staff do not offer support for your local computer and software installations. Other ssh clients may work or not, but beware of clients that create persistent sessions to the DCC.

## SSH Keys 
Setting up ssh keys from your workstation will greatly simplify the login and file transfer process by creating a secure key based session between your workstation and the DCC instead of using your password with MFA.

### step 1
To generate a key pair on a Macintosh or Linux machine (your local device):

`ssh-keygen -t rsa -b 4096`

The public key generated will be stored in the file:

`~/.ssh/id_rsa.pub`

### step 2
Copy your SSH KEYS shows in the terminal. 
`cd .ssh`
`cat id_rsa.pub`

### step 3
Paste your KEYS into your account. To enable ssh public key authentication update your “SSH Public keys” under “Advanced IT Options” at: [idms-web.oit.duke.edu/portal](https://idms-web.oit.duke.edu/portal).

![web](https://user-images.githubusercontent.com/90811429/218135707-44132426-3420-4703-b468-4a9776ad7f7c.png )


![ssh](https://user-images.githubusercontent.com/90811429/218135693-3fe8c16a-e67b-4fb2-860b-379f85faf6ce.png)



View more general information about ssh public key authentication at [ssh.com](https://www.ssh.com/ssh/public-key-authentication#key-pair---public-and-private).

### step 4
Test your connection with the passphrase you setup for ssh keys: `Enter passphrase for key` request should be brought up instead of  `Password`



    netid@node ~ % ssh netid@dcc-login.oit.duke.edu
    Enter passphrase for key '/Users/netid/.ssh/id_rsa': 
     Last login: Tue Dec 21 11:13:41 2021 from xxx
    ################################################################################
    # MOTD                                                                         # 
    # My patch window is wednesday 03:00                                           # ################################################################################
    netid@dcc-login-03  ~ $

### step 5 
For more information about ssh keys check these [Q&A](https://www.ssh.com/academy/ssh)
