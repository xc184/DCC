# Research Computing Support Docs

## About

The Duke Research Computing user support website: https://oit-rc.pages.oit.duke.edu/rcsupportdocs is generated from [MarkDown][url_mkdocs] documents hosted in this repository. The projects static pages are generated using MkDocs and built by Gitlab CI, following the steps defined in .gitlab-ci.yml.

## Editing the website

### Quick edits

For quick edits to existing pages, edit files directly in the docs folder.  Changes will be automatically posted to the website (using Gitlab CI).

### Larger changes

For larger changes to the website:

1. Clone the repository to your local machine
2. Run `pip install -r requirements.txt` in the repository directory
3. Add a the document as a .md file somewhere in the `/docs` folder
4. Add the file name and path under the `nav` header at the bottom of `/mkdocs.yml`
5. Run `mkdocs serve` to view the updated website on your local machine
6. Commit and push to this repository, the public website will update automatically in ~5 minutes

### Creating announcements
To create an announcement, add the following (sample) code to the bottom of `theme/main.html`:
```html
{% block announce %}
    {{ super() }}
    <img style='vertical-align:middle;' src='/rcsupportdocs/img/Infobox_info_icon.svg' width="20">
    <div style='vertical-align:middle; display:inline;'>
        RC Maintenance Outage scheduled for 3/7/22 - 3/11/22, for more information: <a href="https://rc.duke.edu/dcc-changes/">https://rc.duke.edu/dcc-changes/</a>
    </div>
{% endblock %}
```

[comment]: #  (link URLs -----------------------------------------------------)

[url_mkdocs]:           http://mkdocs.org
